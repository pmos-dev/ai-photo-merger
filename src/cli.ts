import { ERgba, IRgb, IRgba } from 'tscommons-es-graphics';

import { CommonsArgs, commonsOutputPercent } from 'nodecommons-es-cli';
import {
		commonsOutputSetDebugging,
		commonsOutputDoing,
		commonsOutputSuccess
} from 'nodecommons-es-cli';
import { commonsFileWriteBinaryFile } from 'nodecommons-es-file';
import { CommonsJimp, CommonsTiffWriter } from 'nodecommons-es-graphics';
import { TCommonsAiGenericTrainingData } from 'nodecommons-es-ai';

import { Merger } from './classes/merger';

import { TXyAb } from './types/txy-ab';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) commonsOutputSetDebugging(true);

async function getMatrix(src: string): Promise<IRgba[][]> {
	commonsOutputDoing(`Loading image ${src}`);
	const image: CommonsJimp = new CommonsJimp();
	await image.loadFromFile(src);
	commonsOutputSuccess();

	commonsOutputDoing(`Converting to RGB matrix`);
	const rgbs: IRgba[][] = await image.toRgbaXyMatrix();
	commonsOutputSuccess();

	return rgbs;
}

async function doTrain(
		net: string,
		aSrc: string,
		bSrc: string,
		hiddenLayers: number[],
		iterations: number,
		duplicates: number = 3,
		subPixelNoise: number = 0.1,
		colorNoise: number = 3 / 255
): Promise<void> {
	const aRgbs: IRgba[][] = await getMatrix(aSrc);
	const bRgbs: IRgba[][] = await getMatrix(bSrc);

	if (aRgbs.length !== bRgbs.length || aRgbs[0].length !== bRgbs[0].length) throw new Error('A and B images are not identical resolution');

	commonsOutputDoing(`Building training data`);
	const trainingData: TCommonsAiGenericTrainingData<TXyAb, IRgb>[] = [];
	for (let x = 0; x < aRgbs.length; x++) {
		for (let y = 0; y < aRgbs[0].length; y++) {
			for (let attempts = duplicates; attempts-- > 0;) {
				// noise for the attempts is done in the painter class

				trainingData.push({
						input: { x: x, y: y, ab: 0 },
						output: {
								[ ERgba.RED ]: aRgbs[x][y][ERgba.RED],
								[ ERgba.GREEN ]: aRgbs[x][y][ERgba.GREEN],
								[ ERgba.BLUE ]: aRgbs[x][y][ERgba.BLUE]
						}
				});

				trainingData.push({
						input: { x: x, y: y, ab: 1 },
						output: {
								[ ERgba.RED ]: bRgbs[x][y][ERgba.RED],
								[ ERgba.GREEN ]: bRgbs[x][y][ERgba.GREEN],
								[ ERgba.BLUE ]: bRgbs[x][y][ERgba.BLUE]
						}
				});
			}
		}
	}
	commonsOutputSuccess();

	const merger: Merger = new Merger(
			aRgbs.length,
			aRgbs[0].length,
			8,	// training is always done on 8-bit images
			subPixelNoise,
			colorNoise
	);
	await merger.train(
			trainingData,
			hiddenLayers,
			iterations,
			undefined,
			true,
			true
	);

	await merger.save(net);
}

async function doPaint(
		net: string,
		ab: number,
		dest: string,
		width: number,
		height: number,
		outputBitsPerSample: number
): Promise<void> {
	const merger: Merger = new Merger(
			width,
			height,
			outputBitsPerSample
	);
	await merger.load(net);

	commonsOutputDoing(`Rendering image`);
	const pixels: number[] = [];
	for (let y = 0; y < height; y++) {
		commonsOutputPercent(y, height);
		for (let x = 0; x < width; x++) {
			const pixel: IRgb = await merger.run({
					x: x,
					y: y,
					ab: ab
			});

			pixels.push(...[ pixel.red, pixel.green, pixel.blue ]);
		}
	}
	commonsOutputSuccess();

	commonsOutputDoing(`Saving result to ${dest}`);

	const tiffWriter: CommonsTiffWriter = new CommonsTiffWriter(
			pixels,
			width,
			height,
			outputBitsPerSample
	);
	tiffWriter.generate();

	commonsFileWriteBinaryFile(dest, tiffWriter.toBuffer());

	commonsOutputSuccess();
}

(async (): Promise<void> => {
	const net: string = args.getString('net');

	if (args.hasAttribute('train')) {
		const aSrc: string = args.getString('a-src');
		const bSrc: string = args.getString('b-src');
		const hiddenLayers: number[] = args.getString('hidden-layers')
				.split(',')
				.map((layer: string): number => parseInt(layer, 10));
		const iterations: number = args.getNumber('iterations');
		const duplicates: number = args.getNumberOrUndefined('pixel-duplicates') || 3;
		const subPixelNoise: number = args.getNumberOrUndefined('sub-pixel-noise') || 0.1;
		const colorNoise: number = args.getNumberOrUndefined('colour-noise') || args.getNumberOrUndefined('color-noise') || (3 / 255);

		await doTrain(
				net,
				aSrc,
				bSrc,
				hiddenLayers,
				iterations,
				duplicates,
				subPixelNoise,
				colorNoise
		);
	}

	if (args.hasAttribute('paint')) {
		const dest: string = args.getString('dest');
		const width: number = args.getNumber('width');
		const height: number = args.getNumber('height');
		const ab: number = args.getNumber('ab');
		const bitsPerSample: number = args.getNumberOrUndefined('bits-per-sample') || 8;

		await doPaint(
				net,
				ab,
				dest,
				width,
				height,
				bitsPerSample
		);
	}
})();
