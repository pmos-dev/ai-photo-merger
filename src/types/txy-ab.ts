import { TXy } from 'tscommons-es-graphics';

export type TXyAb = TXy & { ab: number };
